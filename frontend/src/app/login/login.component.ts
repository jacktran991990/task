import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json',
      'Authorization': 'my-auth-token' 
    })
  };

  loginForm = new FormGroup({
    username: new FormControl(),
    password: new FormControl(),
    remember: new FormControl()
  });
  constructor(private http: HttpClient) { }

  ngOnInit() {
  }

  login() {
    console.log(this.loginForm.value); 
    return this.http.post('http://localhost:1234/server.php', this.loginForm.value, this.httpOptions).subscribe(res => {
      console.log(res)
    });

  }

}
